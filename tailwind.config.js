/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  safelist: [
    {
      pattern: /bg-(indigo|teal|amber|purple)-(100|400|600|700|900)/,
      variants:['hover', 'focus', 'dark', 'dark:hover']
    },
    {
      pattern: /text-(indigo|teal|amber|purple)-(100|400|600|700|900)/,
      variants:['hover', 'focus', 'dark', 'dark:hover']
    },
    {
      pattern: /border-(indigo|teal|amber|purple)-(100|400|600|700|900)/,
      variants:['hover', 'focus', 'dark', 'dark:hover']
    }
  ],
  content: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'class',
  theme: {
    container: {
      screens: {
        xs: '300px',
        sm: '400px',
        md: '500px',
        xl: '800px',
        '2xl': '1150px',
      },
    }
  },
}
