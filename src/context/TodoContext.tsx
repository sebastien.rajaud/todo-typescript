import React, {useEffect, useRef, useState} from "react";
import {v4 as uuid} from 'uuid';
import {
    Status,
    TodoCategoryInterface,
    TodoContextInterface,
    TodoInterface
} from "../interfaces/todo.interface";
import {ProviderInterface} from "../interfaces/common.interface";
import {TodoFormType} from "../types/todo.types";

export const TodoStateContext = React.createContext<TodoContextInterface>({
    todos: [],
    filteredTodos: [],
    categories: [],
    selectedFilter: Status.ALL,
    setSelectedFilter: (status: Status): void => {
    },
    addTodo: (todo: TodoFormType): void => {
    },
    toggleTodo: (todo: string): void => {
    },
    checkTodo: (todo: string): void => {
    },
    updateTodo: (todoId: string, value: TodoFormType): void => {
    },
    deleteTodo: (todoId: string): void => {
    },
    filterTodo: (status): void => {
    },
    addCategory: (categoryName: string): void => {
    },
    deleteCategory: (categoryId: string): void => {
    }
})

interface TodoStore {
    todos: TodoInterface[],
    categories: TodoCategoryInterface[]
}

export const TodoContextProvider: React.FunctionComponent<ProviderInterface> = ({children}) => {
    const mounted = useRef(true);
    const [todos, setTodos] = useState<TodoInterface[]>([]);
    const [categories, setCategories] = useState<TodoCategoryInterface[]>([]);
    const [filteredTodos, setFilteredTodos] = useState<TodoInterface[]>([]);
    const [selectedFilter, setSelectedFilter] = useState<Status>(Status.ALL);

    useEffect(() => {
        if (localStorage.getItem('todos_ts') !== null) {
            const store = getStoredData();
            setTodos(store.todos);
            setCategories(store.categories)
        }
    }, []);
    useEffect(() => {
        if (!mounted.current) {
            return saveToLocalStorage({
                todos,
                categories
            });
        }
        mounted.current = false;

    }, [todos, categories]);
    useEffect(() => {
        const newFilteredTodos: TodoInterface[] = todos.filter(todo => {
            if (selectedFilter === Status.DONE) {
                return todo.done === true ? todo : null
            } else if (selectedFilter === Status.TO_DO) {
                return todo.done === false ? todo : null
            } else {
                return todo;
            }
        })
        setFilteredTodos(newFilteredTodos);
    }, [selectedFilter, todos])

    const getStoredData = () => {
        const storedData: string | null = localStorage.getItem('todos_ts');
        return storedData ? JSON.parse(storedData) : []
    }
    const saveToLocalStorage = (store: TodoStore): void => {
        if (todos || categories) {
            localStorage.setItem('todos_ts', JSON.stringify({todos, categories}))
        }
    }
    const addTodo = (todo: TodoFormType) => {
        const id = uuid();
        const newTodo = {
            id: id,
            title: todo.title,
            desc: todo.desc,
            editMode: false,
            done: false,
            category: todo.category ? todo.category : null
        }
        setTodos([...todos, newTodo]);

    }
    const toggleTodo = (todoId: string) => {
        const newTodos = todos.filter(todo => {
            if (todo.id === todoId) {
                todo.editMode = !todo.editMode
            }
            return todo;
        })
        setTodos(newTodos);
    }
    const deleteTodo = (todoId: string) => {
        const newTodos = todos.filter(todo => todo.id !== todoId)
        setTodos(newTodos);
    }
    const updateTodo = (todoId: string, value: TodoFormType) => {
        const newTodos = todos.filter(todo => {
            if (todo.id === todoId) {
                todo.title = value.title;
                todo.desc = value.desc;
                todo.category = value.category ? value.category : null;
            }
            return todo;
        })
        setTodos(newTodos);
    }
    const checkTodo = (todoId: string) => {
        const newTodos = todos.filter(todo => {
            if (todo.id === todoId) {
                todo.done = !todo.done
            }
            return todo;
        })
        setTodos(newTodos);
    }
    const filterTodo = (status: Status) => {
        let store = getStoredData();
        let todos;
        if (status === Status.DONE) {
            todos = store.todos.filter((todo: TodoInterface) => todo.done === true)
        } else {
            todos = store.todos.filter((todo: TodoInterface) => todo.done !== true)
        }
        setTodos(todos);
    }
    console.log(Math.random().toString(16).substr(-6))
    const addCategory = (categoryName: string): void => {
        const category: TodoCategoryInterface = {
            label: categoryName,
            id: uuid(),
            color: `#${Math.random().toString(16).substr(-6)}`
        }
        setCategories([
            ...categories,
            category
        ])
    }
    const deleteCategory = (categoryId: string): void => {
        const newCategories = categories.filter(category => category.id !== categoryId)
        const newTodos = todos.map( todo => {
            if (todo.category && todo.category.id === categoryId) {
                todo.category = null
            }
            return todo;
        })
        setTodos(newTodos);
        setCategories(newCategories);
    }

    return (
        <TodoStateContext.Provider value={{
            todos,
            categories,
            filteredTodos,
            selectedFilter,
            setSelectedFilter,
            addTodo,
            toggleTodo,
            checkTodo,
            updateTodo,
            deleteTodo,
            filterTodo,
            addCategory,
            deleteCategory
        }}>
            {children}
        </TodoStateContext.Provider>
    )
}