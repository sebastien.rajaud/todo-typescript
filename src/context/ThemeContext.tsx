import React, {useContext, useState} from 'react';
import {ThemeInterface} from "../interfaces/theme.interface";
import {ColorThemeType, ThemeModeType} from "../types/theme.types";
import {ProviderInterface} from "../interfaces/common.interface";

const ThemeContext = React.createContext<ThemeInterface>({
    themeMode: 'light',
    setThemeMode: (mode: ThemeModeType): void => {},
    themePrimaryColor : ColorThemeType.INDIGO,
    setThemePrimaryColor: (color: ColorThemeType): void => {}
});

export const ThemeContextProvider: React.FunctionComponent<ProviderInterface> = ({children}) => {
    const [themeMode, setThemeMode] = useState<ThemeModeType>('light');
    const [themePrimaryColor, setThemePrimaryColor] = useState<ColorThemeType>(ColorThemeType.INDIGO);

    return (
        <ThemeContext.Provider value={{
            themeMode: themeMode,
            setThemeMode,
            themePrimaryColor,
            setThemePrimaryColor
        }}>
            {children}
        </ThemeContext.Provider>
    )
}

export const useTheme = () => useContext(ThemeContext);
