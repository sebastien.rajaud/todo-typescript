import React, {useContext} from 'react';
import './App.css';
import Form from "./components/Form";
import Navbar from "./components/Navbar";
import TodoList from "./components/TodoList";
import Filters from "./components/Filters";
import {useTheme} from "./context/ThemeContext";
import Categories from "./components/Categories";


const App = () => {
    const {themeMode} = useTheme();
    return (
        <div className={themeMode}>
        <div className="w-full h-screen bg-gray-50 dark:bg-gray-800">
            <Navbar/>
            <div className="container m-auto">
                <div className="lg:grid grid-cols-3 h-full gap-10 ">
                    <div className="lg:mb-0 mb-5 col-span-1 p-7 rounded bg-gray-100 dark:bg-gray-700">
                        <Form/>
                    </div>
                    <div className="col-span-2">
                        <Filters/>
                        <Categories />
                        <TodoList/>
                    </div>
                </div>
            </div>
        </div>
        </div>
    );
}

export default App;
