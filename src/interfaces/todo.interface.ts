import {TodoFormType} from "../types/todo.types";

export interface TodoInterface {
    id: string;
    title: string;
    desc?: string;
    done: boolean;
    editMode: boolean;
    category: TodoCategoryInterface | null
}

export interface TodoCategoryInterface {
    id: string;
    label: string;
    color: string;
}

export interface TodoContextInterface {
    todos: TodoInterface[] | [];
    categories: TodoCategoryInterface[] | [];
    filteredTodos: TodoInterface[] | [];
    selectedFilter: Status;
    setSelectedFilter: (status: Status) => void;
    addTodo: (todo: TodoFormType) => void;
    updateTodo: (todoId: string, value: TodoFormType) => void;
    toggleTodo: (todoId: string) => void;
    checkTodo: (todoId: string) => void;
    deleteTodo: (todoId: string) => void;
    filterTodo: (status: Status) => void;
    addCategory: (categoryName: string) => void;
    deleteCategory: (categoryId: string) => void;
}

export enum Status {
    ALL= 'all',
    DONE = 'done',
    TO_DO = 'to_do'
}