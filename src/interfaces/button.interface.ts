import React, {FormEvent, FormEventHandler, MouseEventHandler, ReactEventHandler, ReactNode} from "react";

export interface ButtonInterface {
    children: ReactNode;
    type?: 'submit' | 'button';
    size?: 'sm' | 'md' | 'lg';
    onClick?: (e?: React.FormEvent<HTMLButtonElement> | React.MouseEvent<HTMLButtonElement>) => void;
    buttonType?: 'primary' | 'secondary';
    iconButton?: boolean;
    isActive?: boolean;
    isDisabled?: boolean;
    className?: string;
}