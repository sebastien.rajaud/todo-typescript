import {ColorThemeType, ThemeModeType} from "../types/theme.types";

export interface ThemeInterface {
    themeMode: ThemeModeType;
    setThemeMode: (mode: ThemeModeType) => void;
    themePrimaryColor: ColorThemeType;
    setThemePrimaryColor: (color: ColorThemeType) => void;
}