import React, { useContext, useEffect, useState} from 'react';
import {IoMdArrowDropdown} from "react-icons/io";
import {MdClose} from "react-icons/md";
import {useTheme} from "../context/ThemeContext";
import Button from "./Button";
import {TodoStateContext} from "../context/TodoContext";
import {ThemePalette} from "../Theme";

export interface SelectDataInterface {
    label: string;
    id: string;

}

export interface SelectInterface<T> {
    data: T[];
    className?: string;
    handleSelect: (option: T) => void;
    selected?: T | null
}

const Select = <T extends SelectDataInterface>({data, className, handleSelect, selected}: SelectInterface<T>) => {
    const {themeMode, themePrimaryColor} = useTheme();
    const {addCategory} = useContext(TodoStateContext);
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const [createdData, setCreatedData] = useState<string>('');
    const [selectedOption, setSelectedOption] = useState<T | null>(null);

    useEffect(() => {
        if (selected) {
            setSelectedOption(selected)
        }
    }, [selected]);

    const handleClick = () => {
        setIsOpen(prevState => !prevState)
    }
    const handleChoice = (option: T) => {
        setSelectedOption(option);
        setIsOpen(false);
        handleSelect(option);
    }
    const handleCreateData = (): void => {
        addCategory(createdData);
        setCreatedData('');
    }
    const handleChangeCreatedData = (e: React.FormEvent<HTMLInputElement>): void => {
        const {currentTarget} = e;
        setCreatedData(currentTarget.value)
    }

    return (
        <div className={`rounded  bg-white relative`}>
            <div className={`${className} rounded px-3 py-3 w-full h-[42px] flex justify-between items-center`}
                 onClick={handleClick}>
                <span>{selectedOption && selectedOption.label}</span>
                {isOpen ? <MdClose size={24} color={'gray'}/>  : <IoMdArrowDropdown size={24} color={'gray'}/> }
            </div>
            {isOpen && (
                <div
                    className="bg-white absolute top-full left-0 right-0 shadow-[0_10px_20px_-15px_rgba(0,0,0,0.3)] rounded rounded-t-none">
                    <div className="border border-gray-100 mx-3 mt-3 mb-0 flex justify-between rounded">
                        <input type="text" className="p-2 flex-1 bg-gray-50 rounded-r-none" value={createdData} onChange={handleChangeCreatedData}/>
                        <Button type="button" size="sm" onClick={handleCreateData} className="rounded-l-none">OK</Button>
                    </div>
                    <div className="h-[150px] overflow-y-scroll  pb-2">
                        {data.map(option => <div key={option.id} onClick={() => handleChoice(option)}
                                                 className={`text-sm  px-3 py-3 text-gray-500 hover:bg-${ThemePalette[themePrimaryColor].background[themeMode]} hover:text-gray-100`}>{option.label}</div>)}
                    </div>
                </div>
            )}
        </div>
    );
};

export default Select;