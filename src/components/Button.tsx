import React, {JSXElementConstructor, PropsWithChildren, ReactNode} from 'react';
import {ButtonInterface} from "../interfaces/button.interface";
import {useTheme} from "../context/ThemeContext";
import {ThemePalette} from "../Theme";

const Button = ({
                    children,
                    type = 'submit',
                    size = 'lg',
                    onClick,
                    buttonType = 'primary',
                    iconButton = false,
                    isActive = false,
                    isDisabled = false,
                    className = ''
                }: ButtonInterface) => {

    const {themePrimaryColor, themeMode} = useTheme();

    let paddings: string = '';
    let buttonStyle: string = '';

    switch (size) {
        case 'lg':
            paddings = 'py-3 px-8 text-base';
            break;
        case 'md':
            paddings = 'py-2 px-5 text-base';
            break;
        case 'sm':
            paddings = 'py-1 px-4 text-xs';
            break;
    }

    switch (buttonType) {
        case 'primary':
            buttonStyle = `bg-${ThemePalette[themePrimaryColor].background[themeMode]} hover:bg-${ThemePalette[themePrimaryColor].background['dark']} dark:hover:bg-${ThemePalette[themePrimaryColor].background['light']} ${isActive && 'bg-indigo-900'} text-gray-50`;
            break;
        case 'secondary':
            buttonStyle = `border ${isActive ? `bg-${ThemePalette[themePrimaryColor].background[themeMode]} border-${ThemePalette[themePrimaryColor].background[themeMode]} text-gray-50` : `text-${ThemePalette[themePrimaryColor].text} dark:text-gray-50 border-${ThemePalette[themePrimaryColor].background[themeMode]} dark:hover:bg-${ThemePalette[themePrimaryColor].background['dark']}`} hover:text-white`;
            break;
    }

    return (
        <button type={type}
                onClick={onClick}
                disabled={isDisabled}
                className={`disabled:opacity-25 disabled:cursor-not-allowed cursor-pointer transition-all ${iconButton ? `text-${ThemePalette[themePrimaryColor].background['light']} dark:text-gray-50` : `rounded-md ${buttonStyle} ${paddings} text-center`} font-semibold ${className}`}>
            {children}
        </button>
    );
};

export default Button;