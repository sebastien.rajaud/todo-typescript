import React, {useContext} from 'react';
import {TodoInterface} from "../interfaces/todo.interface";
import Button from "./Button";
import {MdEdit, MdRadioButtonUnchecked, MdCheckCircle, MdDelete} from "react-icons/md";
import {TodoStateContext} from "../context/TodoContext";
import FormToggle from "./FormToggle";
import {useTheme} from "../context/ThemeContext";
import {ThemePalette} from "../Theme";
import Category from "./Category";

const Todo = ({todo}: { todo: TodoInterface }) => {
    const {toggleTodo, checkTodo, deleteTodo} = useContext(TodoStateContext);
    const {themePrimaryColor, themeMode} = useTheme();
    
    const handleDelete = (): void => {
        deleteTodo(todo.id);
    }
    const handleToggle = (): void => {
        toggleTodo(todo.id);
    }
    const handleCheck = (): void => {
        checkTodo(todo.id)
    }

    const handleReset = ():void => {
        handleToggle();
    }

    return (
        <div className="flex items-center gap-5 rounded px-4 py-4 shadow bg-white justify-start dark:bg-gray-700">
            {!todo.editMode && (
                <Button type="button" className="self-start h-[50px]" size="sm" onClick={handleCheck}>
                    {todo.done ? <MdCheckCircle size={20}/> : <MdRadioButtonUnchecked size={20}/>}
                </Button>
            ) }
            {todo.editMode ? <FormToggle todo={todo} handleReset={handleReset}/> : (
                <div onClick={handleToggle} className={`cursor-pointer text-${ThemePalette[themePrimaryColor].text}   font-bold flex-1  dark:font-medium`}>
                    <div className="flex justify-start items-center gap-5 mb-1">
                    <div className="md:text-base text-sm  dark:text-gray-50">{todo.title}</div>
                        {todo.category && <div className="flex mt-0.5"><Category category={todo.category} /></div>}
                    </div>
                    <div className="text-xs font-light text-gray-400">{todo.desc}</div>
                </div>
            )}
            {!todo.editMode && (
                <div className="flex gap-2">
                    <Button iconButton type="button" size="md" onClick={handleToggle}><MdEdit/></Button>
                    <Button iconButton type="button" size="md" onClick={handleDelete}><MdDelete/></Button>
                </div>
            )}
        </div>
    );
};

export default Todo;