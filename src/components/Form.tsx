import React, {useContext, useState} from 'react';
import {TodoStateContext} from "../context/TodoContext";
import Button from "./Button";
import {TodoFormType} from "../types/todo.types";
import Select, {SelectDataInterface} from "./Select";
import {TodoCategoryInterface} from "../interfaces/todo.interface";

const Form = (): JSX.Element => {
    const [value, setValue] = useState<TodoFormType>({title: '', desc: ''});
    const {addTodo, categories} = useContext(TodoStateContext);
    const handleChange = (e: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
        setValue({
            ...value,
            [e.currentTarget.name]: e.currentTarget.value
        })
    }
    const handleSelect = (option: TodoCategoryInterface) => {
        setValue({
            ...value,
            category: option
        })
    }
    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (value.title) {
            addTodo(value);
        }
        setValue({title: '', desc: ''});
    }

    return (
        <div>
            <form onSubmit={handleSubmit} className="items-end gap-0 justify-start">
                <div className="mb-2">
                    <label
                        htmlFor="todo"
                        className="mb-3 block text-lg font-medium text-gray-800 dark:text-gray-50"
                    >
                        Add a Todo
                    </label>
                    <label
                        htmlFor="category"
                        className="mb-0.5 block text-sm font-medium text-gray-800 dark:text-gray-50"
                    >Category
                    </label>
                    <Select<TodoCategoryInterface> data={categories} className="mb-2" handleSelect={handleSelect}/>
                    <label
                        htmlFor="title"
                        className="mb-0.5 block text-sm font-medium text-gray-800 dark:text-gray-50"
                    >Title
                    </label>
                    <input
                        type="text"
                        name="title"
                        id="title"
                        placeholder="Add a todo to your list"
                        value={value.title}
                        onChange={handleChange}
                        className="w-full rounded-md mb-2 transition-all border-gray-300 bg-white py-3 px-3 text-base font-medium text-gray-800 outline-none focus:border-[#6A64F1] focus:shadow-md"
                    />
                    <label
                        htmlFor="desc"
                        className="mb-0.5 block text-sm font-medium text-gray-800 dark:text-gray-50"
                    >Description
                    </label>
                    <textarea name="desc" rows={5} value={value.desc}
                              className="w-full p-2 rounded border-gray-300 bg-white" onChange={handleChange}/>
                </div>

                <Button
                    className="w-full"
                    isDisabled={!value.title}
                    type="submit">OK</Button>
            </form>
        </div>
    );
};

export default Form;