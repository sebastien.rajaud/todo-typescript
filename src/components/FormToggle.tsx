import React, {FormEvent, ReactNode, useContext, useState} from 'react';
import {TodoCategoryInterface, TodoInterface} from "../interfaces/todo.interface";
import Button from "./Button";
import {TodoStateContext} from "../context/TodoContext";
import {TodoFormType} from "../types/todo.types";
import Select, {SelectDataInterface} from "./Select";


const FormToggle = ({todo, handleReset}: { todo: TodoInterface, handleReset?: () => void }): JSX.Element => {
    const [values, setValues] = useState<TodoFormType>({title: todo.title, desc: todo.desc, category: todo.category});
    const {updateTodo, toggleTodo, categories} = useContext(TodoStateContext);
    const handleChange = (e: FormEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
        setValues({
            ...values,
            [e.currentTarget.name]: e.currentTarget.value
        })
    }
    const handleSubmit = (e: FormEvent<HTMLFormElement>): void => {
        e.preventDefault();
        handleClick();
    }

    const handleClick = (): void => {
        updateTodo(todo.id, values);
        toggleTodo(todo.id);
    }

    const handleSelect = (option: TodoCategoryInterface) => {
        setValues({
            ...values,
            category: option
        })
    }

    const handleCancel = () => {
        handleReset && handleReset();
    }

    return (
        <form onSubmit={handleSubmit} className="flex gap-2 justify-between w-full items-end">
            <div className="flex-1 flex-wrap grid gap-2 grid-cols-2">
                <input type="text" name="title" value={values.title}
                       className="col-span-1 border border-gray-100 w-full p-2 rounded" onChange={handleChange}/>
                <Select<TodoCategoryInterface> data={categories} className="col-span-1 border border-gray-100" handleSelect={handleSelect}
                        selected={values.category && values.category}/>
                <textarea name="desc" value={values.desc}
                          className="col-span-2 p-2 rounded border border-gray-100 w-full " onChange={handleChange}/>
            </div>
            <div>
                <Button type="submit" buttonType="secondary" size="md" onClick={handleCancel}
                        className="block min-w-[150px] mb-2">
                    Cancel
                </Button>
                <Button type="submit" size="md" onClick={handleClick} className="block min-w-[150px]">
                    Save
                </Button>
            </div>
        </form>
    );
};

export default FormToggle;