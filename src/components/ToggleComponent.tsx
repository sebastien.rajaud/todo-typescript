import React, { useState} from 'react';
import  {useTheme} from "../context/ThemeContext";
import './ToggleComponent.css';

interface ToggleComponentType {
    label?: string;
}

const ToggleComponent = ({label}: ToggleComponentType) => {
    const [checked, setChecked] = useState(false);
    const {setThemeMode, themeMode} = useTheme();

    const handleClick = () : void => {
        setChecked(checked => !checked);
        if (setThemeMode) {
            if (themeMode === 'light') {
                setThemeMode('dark')
            } else {
                setThemeMode('light')
            }
        }
    }

    return (
        <>
            <div className="relative inline-block w-10 mr-2 align-middle select-none transition duration-1000 ease-in"
            >
                <input type="checkbox"
                       name="toggle"
                       id="toggle"
                       onChange={handleClick}

                       checked={checked}
                       className={`toggle-checkbox transition duration-400 absolute block w-6 h-6 rounded-full bg-white border-4 appearance-none cursor-pointer`}
                />
                <label htmlFor="toggle"
                       className="toggle-label transition duration-400 block overflow-hidden h-6 rounded-full bg-gray-300 dark:bg-gray-800 dark:border dark:border-gray-400  cursor-pointer"
                ></label>
            </div>
            {label && `<label htmlFor="toggle" className="text-xs text-gray-700">${label}</label>`}
        </>
    );
};

export default ToggleComponent;