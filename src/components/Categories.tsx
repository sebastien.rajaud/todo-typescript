import React, {useContext} from 'react';
import {TodoStateContext} from "../context/TodoContext";
import Category from "./Category";

const Categories: React.FunctionComponent = () => {
    const {categories} = useContext(TodoStateContext);
    return (
        <div className="flex gap-3 mb-3">
            {categories.map(category => <Category category={category} isEditable/>)}
        </div>
    );
};

export default Categories;