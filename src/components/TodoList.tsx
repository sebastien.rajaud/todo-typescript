import React, {useContext} from 'react';
import { TodoInterface} from "../interfaces/todo.interface";
import Todo from "./Todo";
import {TodoStateContext} from "../context/TodoContext";

const TodoList = () => {
    const {filteredTodos} = useContext(TodoStateContext);

    return (
        <div className="flex flex-col gap-2">
            {filteredTodos && filteredTodos.map((todo: TodoInterface, index: number) => <Todo key={todo.id} todo={todo}/>)}
        </div>
    );
};

export default TodoList;