import React, {useContext, useState} from 'react';
import {ThemePalette} from "../Theme";
import {useTheme} from "../context/ThemeContext";


const ColorPicker = () => {

    const {themeMode, themePrimaryColor, setThemePrimaryColor} = useTheme();
    const [editMode, setEditMode] = useState<boolean>(false);

    const handleEditMode = (): void => {
        setEditMode(!editMode);
    }

    const handleClick = (event: React.MouseEvent, item: any): void => {
        setThemePrimaryColor(item);
        setEditMode(false);
    }

    return (
        <div className="relative cursor-pointer  ">
            <div onClick={handleEditMode}>
                <div
                    className={`w-8 h-8 bg-${ThemePalette[themePrimaryColor].background[themeMode]}  border border-white  transition-all rounded-full block ring-[#f6cda8] hover:ring-2 ring-offset-1 `}></div>
            </div>
            {editMode && <div
                className="absolute  mt-1 flex flex-col gap-1 p-2 top-full left-1/2 -translate-x-1/2 drop-shadow-md dark:bg-gray-800 bg-white rounded-md">
                {Object.entries(ThemePalette).map(item => {
                    return (
                        <div
                            onClick={(e: React.MouseEvent) => handleClick(e, item[0])}
                            className={`w-8 h-8  bg-${ThemePalette[item[0]].background[themeMode]} transition-all rounded-full block ring-[#f6cda8] hover:ring-2 ring-offset-1 `}></div>
                    )
                })}
            </div>}
        </div>
    );
};

export default ColorPicker;