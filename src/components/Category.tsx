import React, {useContext} from 'react';
import {TodoCategoryInterface} from "../interfaces/todo.interface";
import {TodoStateContext} from "../context/TodoContext";
import {MdClose} from "react-icons/md";

interface CategoryComponentInterface {
    category: TodoCategoryInterface
    isEditable?: boolean;
}

const Category = ({category, isEditable = false}: CategoryComponentInterface) => {
    const {deleteCategory} = useContext(TodoStateContext);
    const {color} = category;
    const handleDelete = (): void => {
        deleteCategory(category.id)
    }

    return (
        <div
            style={{
                backgroundColor: color
            }}
            className={`relative rounded-xl flex items-center text-[10px] text-white font-light py-0.5 inline-block overflow-hidden  ${isEditable ? 'pl-2 pr-[20px]' : 'px-2'}`}>
            {category.label}
            {isEditable && <MdClose
                style={{
                    backgroundColor: color
                }}
                onClick={handleDelete} className="absolute right-0 px-1 w-[20px] text-white h-full cursor-pointer "/>}
        </div>
    );
};

export default Category;