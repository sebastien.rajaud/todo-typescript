import React from 'react';
import logo from '../logo.svg'
import {useTheme} from "../context/ThemeContext";
import ToggleComponent from "./ToggleComponent";
import {ThemePalette} from "../Theme";
import ColorPicker from "./ColorPicker";

const Navbar = () => {
    const {setThemeMode, themeMode, themePrimaryColor} = useTheme();
    const handleTheme = () => {
        if (themeMode === "light") {
            setThemeMode('dark')
        } else {
            setThemeMode('light');
        }
    }

    return (
        <div
            className={`bg-${ThemePalette[themePrimaryColor].background.light} dark:bg-${ThemePalette[themePrimaryColor].background.dark} py-1 mb-16`}>
            <div className="flex container m-auto justify-between items-center ">
                <div className="w-20">
                    <img src={logo} className="w-28 object-fill"/>
                </div>
                <div className="flex gap-2 items-center">
                    <ColorPicker/>
                    <ToggleComponent/>
                </div>
            </div>
        </div>
    );
};

export default Navbar;