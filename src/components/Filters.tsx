import React, {useContext} from 'react';
import Button from "./Button";
import {Status} from "../interfaces/todo.interface";
import {TodoStateContext} from "../context/TodoContext";

const Filters = () => {

    const {setSelectedFilter, selectedFilter} = useContext(TodoStateContext);
    const handleFilter = (status : Status) : void => {
        setSelectedFilter(status);
    }

    return (
        <div className="flex gap-2 mb-5">
            <Button type="button" buttonType="secondary" size="sm" isActive={selectedFilter === Status.ALL} onClick={() => handleFilter(Status.ALL)}>ALL</Button>
            <Button type="button" buttonType="secondary" size="sm" isActive={selectedFilter === Status.DONE} onClick={() => handleFilter(Status.DONE)}>DONE</Button>
            <Button type="button" buttonType="secondary" size="sm" isActive={selectedFilter === Status.TO_DO} onClick={() => handleFilter(Status.TO_DO)}>TO DO</Button>
        </div>
    );
};

export default Filters;