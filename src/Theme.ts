interface ThemePaletteInterface {
    [key: string]: {
        background: {
            dark: string,
            light: string
        },
        text: string
    }
}

export const ThemePalette: ThemePaletteInterface = {
    indigo: {
        background: {
            dark: 'indigo-400',
            light: 'indigo-700',
        },
        text: 'indigo-900'
    },
    teal: {
        background: {
            dark: 'teal-400',
            light: 'teal-600',
        },
        text: 'teal-900'
    },
    amber: {
        background: {
            dark: 'amber-400',
            light: 'amber-600',
        },
        text: 'amber-900'
    },
    purple: {
        background: {
            dark: 'purple-400',
            light: 'purple-600',
        },
        text: 'purple-900'
    },
}