import {TodoCategoryInterface} from "../interfaces/todo.interface";

export type TodoFormType = {
    title: string,
    desc?: string,
    category?: TodoCategoryInterface | null
}
