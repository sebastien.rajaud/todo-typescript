export type ThemeModeType = 'dark' | 'light'
export enum ColorThemeType  {
    INDIGO = 'indigo',
    TEAL = 'teal',
    AMBER = 'amber',
    PURPLE = 'purple'
}